!/bin/bash

#UBUNTU USER 
sudo apt-get update -y  && sudo apt-get install apt-transport-https -y 
#UBUNTU USER END

#ROOT USER
sudo su -c "curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -"

sudo su -c "cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF"

sudo su -c "apt-get update"

sudo su -c "swapoff -a"

sudo su -c "sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab"

sudo su -c "modprobe br_netfilter"

sudo su -c "sysctl -p"

sudo su -c "sysctl net.bridge.bridge-nf-call-iptables=1"

sudo su -c "apt-get install docker.io -y"

sudo su -c "cat <<EOF >/etc/docker/daemon.json
{\"exec-opts\": [\"native.cgroupdriver=systemd\"]}
EOF"

sudo su -c "usermod -aG docker ubuntu"

sudo su -c "systemctl restart docker"

sudo su -c "systemctl enable docker.service"
#ROOT USER END

#UBUNTU USER
sudo apt-get install -y kubelet kubeadm kubectl #kubernetes-cni

sudo systemctl daemon-reload

sudo systemctl start kubelet

sudo systemctl enable kubelet.service

#sudo mv aws.yml /etc/kubernetes/

echo -n "sudo hostnamectl set-hostname " >> hostname.sh

sudo curl http://169.254.169.254/latest/meta-data/local-hostname -w "\n" >> hostname.sh

sudo chmod +x ./hostname.sh

sudo ./hostname.sh

sudo cp aws.yml /etc/kubernetes/

#UBUNTU USER END

#ROOT USER
sudo su -c "kubeadm init --config /etc/kubernetes/aws.yml --ignore-preflight-errors=all"
#ROOT USER END

#UBUNTU USER
mkdir -p $HOME/.kube

sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config

sudo chown ubuntu:ubuntu $HOME/.kube/config

sudo kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml


sudo kubeadm token create --print-join-command > kube-join.sh

#UBUNTU USER END

#-------AWS.YML---------
apiVersion: kubeadm.k8s.io/v1beta2
kind: ClusterConfiguration
networking:
  serviceSubnet: "10.100.0.0/16"
  podSubnet: "10.244.0.0/16"
apiServer:
  extraArgs:
    cloud-provider: "aws"
controllerManager:
  extraArgs:
    cloud-provider: "aws"
#-------AWS.YML---------

