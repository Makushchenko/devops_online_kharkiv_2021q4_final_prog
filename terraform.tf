resource "aws_vpc" "epam-vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = "true"

  tags = {
    Name = "epam-vpc"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }

}

resource "aws_default_security_group" "epam-nsg-def" {
  vpc_id      = aws_vpc.epam-vpc.id

  ingress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }


  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "epam-secgr"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }
}

resource "aws_subnet" "epam-subnet" {
  vpc_id            = aws_vpc.epam-vpc.id
  cidr_block        = "10.0.0.0/16"
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = "true"

  tags = {
    Name = "epam-subnet"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }

}

resource "aws_instance" "epam-ins-master" {
  ami           = "ami-0e472ba40eb589f49"
  instance_type = "t2.medium"
  key_name = aws_key_pair.epam-key.key_name
  associate_public_ip_address = true
  subnet_id = aws_subnet.epam-subnet.id
  iam_instance_profile = "epam-role"

  tags = {
    Name = "epam-master"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }
}

resource "null_resource" "epam-exec-master" {

  connection {
    type = "ssh"
    user = "ubuntu"
    port = 22
    agent = false
    host = aws_instance.epam-ins-master.public_ip
    private_key = file("epam-key.pem")
  }

  provisioner "file" {
    source      = "/home/makushchenko/epam-proj/kube-ins.sh"
    destination = "/home/ubuntu/kube-ins.sh"
  }

  provisioner "file" {
    source      = "/home/makushchenko/epam-proj/doc-img.sh"
    destination = "/home/ubuntu/doc-img.sh"
  }

  provisioner "file" {
    source      = "/home/makushchenko/epam-proj/aws.yml"
    destination = "/home/ubuntu/aws.yml"
  }


  provisioner "remote-exec" {
    inline = [
      "sudo chmod +x /home/ubuntu/kube-ins.sh",
      "sudo chmod +x /home/ubuntu/doc-img.sh",
      "sudo /home/ubuntu/kube-ins.sh",
      "sudo /home/ubuntu/doc-img.sh",
    ]
  }
}

resource "time_sleep" "wait-master" {
  depends_on = [null_resource.epam-exec-master]

  create_duration = "20s"
}


resource "aws_instance" "epam-ins-work1" {
  depends_on = [time_sleep.wait-master]

  ami           = "ami-0e472ba40eb589f49"
  instance_type = "t2.small"
  key_name = aws_key_pair.epam-key.key_name
  associate_public_ip_address = true
  subnet_id = aws_subnet.epam-subnet.id
  iam_instance_profile = "epam-role-worker"

  tags = {
    Name = "epam-work"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }
}

resource "aws_instance" "epam-ins-work2" {
  depends_on = [time_sleep.wait-master]

  ami           = "ami-0e472ba40eb589f49"
  instance_type = "t2.small"
  key_name = aws_key_pair.epam-key.key_name
  associate_public_ip_address = true
  subnet_id = aws_subnet.epam-subnet.id
  iam_instance_profile = "epam-role-worker"

  tags = {
    Name = "epam-work"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }
}

resource "aws_instance" "epam-ins-petclinic" {
  depends_on = [time_sleep.wait-master]

  ami           = "ami-0e472ba40eb589f49"
  instance_type = "t2.micro"
  key_name = aws_key_pair.epam-key.key_name
  associate_public_ip_address = true
  subnet_id = aws_subnet.epam-subnet.id
  iam_instance_profile = "epam-role-worker"

  tags = {
    Name = "petclinic"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }
}
resource "time_sleep" "wait-work" {
  depends_on = [aws_instance.epam-ins-work1,aws_instance.epam-ins-work2,aws_instance.epam-ins-petclinic]

  create_duration = "30s"
}


resource "null_resource" "epam-exec-work1" {
  depends_on = [time_sleep.wait-work]

  connection {
    type = "ssh"
    user = "ubuntu"
    port = 22
    agent = false
    host = aws_instance.epam-ins-work1.public_ip
    private_key = file("epam-key.pem")
  }

  provisioner "local-exec" {
      command = "sudo chmod 400 epam-key.pem; sudo scp -3 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i epam-key.pem ubuntu@${aws_instance.epam-ins-master.public_ip}:/home/ubuntu/kube-join.sh ubuntu@${aws_instance.epam-ins-work1.public_ip}:/home/ubuntu/; sudo chmod 644 epam-key.pem"
  }


  provisioner "file" {
    source      = "/home/makushchenko/epam-proj/kube-ins-w.sh"
    destination = "/home/ubuntu/kube-ins-w.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo chmod +x /home/ubuntu/kube-ins-w.sh",
      "sudo chmod +x /home/ubuntu/kube-join.sh",
      "sudo /home/ubuntu/kube-ins-w.sh",
      "sudo su -c \"/home/ubuntu/kube-join.sh\"",
    ]
  }
}

resource "null_resource" "epam-exec-petclinic" {
  depends_on = [time_sleep.wait-work]

  connection {
    type = "ssh"
    user = "ubuntu"
    port = 22
    agent = false
    host = aws_instance.epam-ins-petckinic.public_ip
    private_key = file("epam-key.pem")
  }

  provisioner "local-exec" {
      command = "sudo chmod 400 epam-key.pem; sudo scp -3 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i epam-key.pem ubuntu@${aws_instance.epam-ins-master.public_ip}:/home/ubuntu/kube-join.sh ubuntu@${aws_instance.epam-ins-petclinic.public_ip}:/home/ubuntu/; sudo chmod 644 epam-key.pem"
  }


  provisioner "file" {
    source      = "/home/makushchenko/epam-proj/kube-ins-w.sh"
    destination = "/home/ubuntu/kube-ins-w.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo chmod +x /home/ubuntu/kube-ins-w.sh",
      "sudo chmod +x /home/ubuntu/kube-join.sh",
      "sudo /home/ubuntu/kube-ins-w.sh",
      "sudo su -c \"/home/ubuntu/kube-join.sh\"",
    ]
  }
}

resource "null_resource" "epam-exec-work2" {
  depends_on = [time_sleep.wait-work]

  connection {
    type = "ssh"
    user = "ubuntu"
    port = 22
    agent = false
    host = aws_instance.epam-ins-work2.public_ip
    private_key = file("epam-key.pem")
  }

  provisioner "local-exec" {
      command = "sudo chmod 400 epam-key.pem; sudo scp -3 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i epam-key.pem ubuntu@${aws_instance.epam-ins-master.public_ip}:/home/ubuntu/kube-join.sh ubuntu@${aws_instance.epam-ins-work2.public_ip}:/home/ubuntu/; sudo chmod 644 epam-key.pem"
  }


  provisioner "file" {
    source      = "/home/makushchenko/epam-proj/kube-ins-w.sh"
    destination = "/home/ubuntu/kube-ins-w.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo chmod +x /home/ubuntu/kube-ins-w.sh",
      "sudo chmod +x /home/ubuntu/kube-join.sh",
      "sudo /home/ubuntu/kube-ins-w.sh",
      "sudo su -c \"/home/ubuntu/kube-join.sh\"",
    ]
  }
}

resource "tls_private_key" "epam-pk" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "epam-key" {
  key_name   = "epam-key"
  public_key = tls_private_key.epam-pk.public_key_openssh

  provisioner "local-exec" {
    command = "echo '${tls_private_key.epam-pk.private_key_pem}' > ./epam-key.pem"
  }
}


resource "aws_internet_gateway" "epam-gw" {
  vpc_id = aws_vpc.epam-vpc.id

  tags = {
    Name = "epam-gw"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }

}

resource "aws_default_route_table" "epam-df-rt" {
  default_route_table_id = aws_vpc.epam-vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.epam-gw.id
  }

  tags = {
    Name = "epam-def-rt"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }

}
