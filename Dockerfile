FROM openjdk:8-jre-alpine
RUN mkdir /app && addgroup -S makushchenko && adduser -S -s /bin/false -G makushchenko makushchenko
WORKDIR /app
ARG build_id
ADD http://art.internalpro.site/artifactory/example-repo-local/org/springframework/samples/spring-petclinic/2.6.${build_id}-SNAPSHOT/spring-petclinic-2.6.${build_id}-SNAPSHOT.jar /app.jar
RUN chown -R makushchenko:makushchenko /app.jar && ls -la
USER makushchenko
EXPOSE 8080
CMD ["java", "-jar", "/app.jar"]
