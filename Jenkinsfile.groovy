node {
    def server = Artifactory.server 'artifactory'
    def buildMaven = Artifactory.newMavenBuild()
    def buildInfo
    String IMAGE_BASE = 'makushchenko/pet-clinic'
    String IMAGE_TAG = "v$BUILD_NUMBER"
    String IMAGE_NAME = "pet-clinic:latest"
    String DOCKERFILE_NAME = "docker/Dockerfile"
    String WORKDIR = 'spring-petclinic'
    String ECR_URL = '346832455654.dkr.ecr.us-east-1.amazonaws.com'
    
    environment {
        SONAR_HOME = "${tool name: 'sonar-scanner', type: 'hudson.plugins.sonar.SonarRunnerInstallation'}"
    }

    try{
        stage('Clone Pet Clinic GitLab') {
            checkout([$class: 'GitSCM', branches: [[name: 'main']], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'pipe-git-token', url: 'https://gitlab.com/Makushchenko/devops_online_kharkiv_2021q4_final_prog.git']]])
        }
        stage('Build artifact and upload to Artifactory storage') {
            dir(WORKDIR) {
                buildMaven.tool = 'mvn'
                buildMaven.resolver releaseRepo: 'SNAPSHOTS', snapshotRepo: 'SNAPSHOTS', server: server
                buildInfo = Artifactory.newBuildInfo()
                buildMaven.deployer releaseRepo: 'libs-release-local', snapshotRepo: 'example-repo-local', server: server
                buildMaven.run pom: 'pom.xml', goals: 'clean install -Dbuild_id=${BUILD_NUMBER}', buildInfo: buildInfo
            }
        }
        /*stage('Build artifact and upload to Artifactory storage') {
            dir(WORKDIR) {
                buildMaven.run pom: 'pom.xml', goals: 'clean install -Dbuild_id=${BUILD_NUMBER}', buildInfo: buildInfo
            }
        }*/
        stage('SonarQube tests') {
            scannerHome = tool 'sonar-scanner'
            withSonarQubeEnv('sonar') {
                sh """${scannerHome}/bin/sonar-scanner"""
            }
        }
        stage('Quality Gate') {
            timeout(time: 3, unit: 'MINUTES') {
                waitForQualityGate abortPipeline: true
            }
        }
        podTemplate(containers: [
                containerTemplate(name: 'jnlp', image: 'jeanycyang/docker-jnlp-slave', args: '${computer.jnlpmac} ${computer.name}', workingDir: '/var/jenkins_home', privileged: false)
        ], volumes: [hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock')]) {
            node(POD_LABEL) {
                stage('Build image from artifact Pet Clinic and Push to AWS ECR') {
                    checkout([$class: 'GitSCM', branches: [[name: 'main']],
                              doGenerateSubmoduleConfigurations: false, extensions: [],
                              submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'pipe-git-token',
                                                                     url: 'https://gitlab.com/Makushchenko/devops_online_kharkiv_2021q4_final_prog.git']]])
                    withCredentials([usernamePassword(credentialsId: 'aws-login', passwordVariable: 'password', usernameVariable: 'username')]) {
                        sh """
                            ls -al
                            docker build --build-arg build_id=${BUILD_NUMBER} -t ${ECR_URL}/pet-clinic:${BUILD_NUMBER} .
                            docker login --username ${username} --password ${password} ${ECR_URL}
                            docker push ${ECR_URL}/pet-clinic:${BUILD_NUMBER}
                            ls
                            docker images
                            docker rmi ${ECR_URL}/pet-clinic:${BUILD_NUMBER}
                            docker images
                        """
                    }

                }
            }
        }
        stage('Deploy PetClinic'){
            withKubeConfig([credentialsId: 'jenkins-login', serverUrl: 'https://10.0.6.164:6443']) {
                sh 'ls -al'
                sh 'cat petdeploy.yaml | sed "s/{{BUILD_ID}}/${BUILD_NUMBER}/g" | kubectl apply -f -'
                sh 'cat petdeploy.yaml'
            }
        }
        stage("Success build notification"){
            withCredentials([string(credentialsId: 'tel-tok', variable: 'TOKEN'), string(credentialsId: 'tel-chat', variable: 'CHAT_ID')]) {
                sh  ("""
                    curl -s -X POST https://api.telegram.org/bot${TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d parse_mode=markdown -d text='*${env.JOB_NAME}* : POC *Branch*: main *Build* : OK *Published* = YES'
                """)
            }

        }
    } catch (e) {
        withCredentials([string(credentialsId: 'tel-tok', variable: 'TOKEN'), string(credentialsId: 'tel-chat', variable: 'CHAT_ID')]) {
            sh  ("""
                curl -s -X POST https://api.telegram.org/bot${TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d parse_mode=markdown -d text='*${e}* : POC *Branch*: main *Build* : `not OK` *Published* = `no`'
            """)
        }
        println("${e}")
    }
}

