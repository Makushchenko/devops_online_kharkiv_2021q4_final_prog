#!/bin/bash

#UBUNTU USER 
sudo apt-get update -y  && sudo apt-get install apt-transport-https -y 
#UBUNTU USER END

#ROOT USER
sudo su -c "curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -"

sudo su -c "cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF"

sudo su -c "apt-get update"

sudo su -c "swapoff -a"

sudo su -c "sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab"

sudo su -c "modprobe br_netfilter"

sudo su -c "sysctl -p"

sudo su -c "sysctl net.bridge.bridge-nf-call-iptables=1"

sudo su -c "apt-get install docker.io -y"

sudo su -c "cat <<EOF >/etc/docker/daemon.json
{\"exec-opts\": [\"native.cgroupdriver=systemd\"]}
EOF"

sudo su -c "usermod -aG docker ubuntu"

sudo su -c "systemctl restart docker"

sudo su -c "systemctl enable docker.service"
#ROOT USER END

#UBUNTU USER
sudo apt-get install -y kubelet kubeadm kubectl #kubernetes-cni

sudo systemctl daemon-reload

sudo systemctl start kubelet

sudo systemctl enable kubelet.service

echo -n "sudo hostnamectl set-hostname " >> hostname.sh

sudo curl http://169.254.169.254/latest/meta-data/local-hostname -w "\n" >> hostname.sh

sudo chmod +x ./hostname.sh

sudo ./hostname.sh

sudo su -c " cat <<EOF >/etc/kubernetes/node.yml
---
apiVersion: kubeadm.k8s.io/v1beta2
kind: JoinConfiguration
discovery:
  bootstrapToken:
    token: "yzcf5r.j3dba0ro5dy01245"
    apiServerEndpoint: "10.0.134.167:6443"
    caCertHashes:
      - "sha256:ce2b7521e4a1cbf776c44f0806fdda1e3622843527495d94b0847d89f31b4975"
nodeRegistration:
  name: ip-10-0-231-224.ec2.internal
  kubeletExtraArgs:
    cloud-provider: aws

EOF"

sudo apt update
sudo apt install unzip
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 267043079553.dkr.ecr.us-east-1.amazonaws.com
#docker pull 267043079553.dkr.ecr.us-east-1.amazonaws.com/hello-epam:latest

#UBUNTU USER END
